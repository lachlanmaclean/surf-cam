const fetch = require("node-fetch");
const fs = require("fs");
const firebase = require('firebase').initializeApp({
    serviceAccount: "./service-account.json",
    databaseURL: "https://surf-api-cache.firebaseio.com"

});
const jsonParse = async function()
{
    var variable = 8;
    firebase.database().goOnline();
    let data =  JSON.parse(fs.readFileSync("object.json", "utf8")); //reading the local file, so we dont use all our api requests
    // simulates the api data response
    let hour = 14;
    const hourRef = firebase.database().ref(`hours`);

    return await hourRef.child(`${hour}`).child("swellHeight").child("0").child("value").on("value", function(snapshot){
        height = snapshot.val();
        firebase.database().goOffline();

    });
     return variable;



};
const aSyncExample = async () =>{
    let hourRef = firebase.database().ref(`hours`);
    return await hourRef.child("14").child("swellHeight").child("0").child("value").on("value", function(snapshot) {
        firebase.database().goOffline();
        height = snapshot.val();
    });
};



