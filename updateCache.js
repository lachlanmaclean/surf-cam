const fetch = require("node-fetch");
const fs = require("fs");
const firebase = require('firebase').initializeApp({
    serviceAccount: "./service-account.json",
    databaseURL: "https://surf-api-cache.firebaseio.com"

});

//Co ordinates of desired location. Default is set @ Waikuku Beach, North Canterbury
const lat = -43.288182;
const lng = 172.722817;

//API Parameters are selected here. A list of available parameters can be found at https://docs.stormglass.io
const params = 'swellHeight,waterTemperature,windDirection,windSpeed,wavePeriod';
const today = new Date();
const timeNow = new Date().toISOString();
//Insert API key here. Bad practice to leave here in plain text
const api = '8adf2800-4f5b-11e9-869f-0242ac130004-8adf28f0-4f5b-11e9-869f-0242ac130004';

//updateCache() grabs the latest 24 hours worth of data from Stormglass.io. Surf conditions can change very quickly

const updateCache = async function(){  //async function because the api needs some time to return the values
    today.setHours(today.getHours() + 3); //I only want today's values and 4 hours
    const end = today.toISOString(); //end parameter denotes I only want data from midnight to 24 hours into the future

    let response = await fetch(`https://api.stormglass.io/point?lat=${lat}&lng=${lng}&params=${params}&end=${end}&start=${timeNow}`, {
        headers: {
            'Authorization': `${api}` //pass the api key from above
        }
    });

        const jsonData = await response.json(); //await the response from the server and then write data to file

        fs.writeFile("./object.json", JSON.stringify(jsonData, null, 4), (err) => {
            if (err) { //error catching
                console.error(err);
                return;
            }

            console.log("Local updated successfully") //Display that we have updated the cache
        });
    console.log("Attempting to contact Firebase Database");
    console.log(`Start time: ${timeNow}, End time: ${end}`);
    await firebase.database().ref('/').set(jsonData);
    console.log("Database updated successfully");
    firebase.database().goOffline();
    console.log("Connection closed");
};

const uploadCache = async function()
{
    let data =  JSON.parse(fs.readFileSync("object.json", "utf8")); //reading the local file, so we dont use all our api requests
    // simulates the api data response

    await firebase.database().ref('/').set(data);
    console.log("Database updated");
    firebase.database().goOffline();
};

//updateCache();//run the code

updateCache();





