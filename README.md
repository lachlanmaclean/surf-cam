# Surf Cam 

Web application written in Node.js backend with Firebase intergration as cache server

Deployed at [https://surf-and-turf.herokuapp.com/](https://surf-and-turf.herokuapp.com/)

## Getting Started

These instructions will help you get set up running the code on your development environment ready for deployment

### Prerequisites

Must have Node installed on your machine. Check [Nodejs.org](http://www.Nodejs.org/) for installation instructions for your Operating system


### Installing

Clone repo and run 

```
git clone https://gitlab.com/lachlanmaclean/surf-cam.git
```
Run npm install to download dependancies

```
npm install
```
#
### Web server

```
node server.js
```
Webserver is served to [http://localhost:8080](http://localhost:8080)


## Authors

* **Lachlan Maclean** - *Surf Cam* - [lachlanmaclean](https://gitlab.com/lachlanmaclean/surf-cam)

## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/lachlanmaclean/surf-cam/blob/master/LICENSE) file for details
