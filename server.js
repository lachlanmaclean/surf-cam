const express = require('express');
const fetch = require("node-fetch");
const fs = require("fs");
const app = express();
const port = process.env.PORT || 8080;
const firebase = require('firebase').initializeApp({
	serviceAccount: "./service-account.json",
	databaseURL: "https://surf-api-cache.firebaseio.com"
});
let localCache = 0;
let x = 0;
let direction ="N";
let ref = firebase.database().ref(`hours`);
app.use(express.static('public')); //Allows access to public folder, for resources
app.set('view engine', 'ejs');
//Co ordinates of desired location. Default is set @ Waikuku Beach, North Canterbury
const lat = -43.288182;
const lng = 172.722817;
//API Parameters are selected here. A list of available parameters can be found at https://docs.stormglass.io
const params = 'swellHeight,waterTemperature,windDirection,windSpeed,wavePeriod';
const today = new Date();
const timeNow = new Date().toISOString();
//Insert API key here. Bad practice to leave here in plain text
const api = '8adf2800-4f5b-11e9-869f-0242ac130004-8adf28f0-4f5b-11e9-869f-0242ac130004';
const updateCache = async function(){  //async function because the api needs some time to return the values
	today.setHours(today.getHours() + 3); //I only want today's values and 4 hours
	const end = today.toISOString(); //end parameter denotes I only want data from midnight to 24 hours into the future
	let response = await fetch(`https://api.stormglass.io/point?lat=${lat}&lng=${lng}&params=${params}&end=${end}&start=${timeNow}`, {
		headers: {
			'Authorization': `${api}` //pass the api key from above
		}
	});
	console.log('API Handshake Confirmed');
	const jsonData = await response.json(); //await the response from the server and then write data to file
	console.log("Attempting to contact Firebase Database");
	await firebase.database().ref('/').set(jsonData);
	console.log("Database updated successfully");
	console.log("Connection closed");
};
const checkUpdate = async function() {
	let timeNow = new Date();
	const ref = firebase.database().ref(`hours`);
	await ref.child("0").child("time").once('value', function (snapshot) {
		let hourOne = new Date(snapshot.val());
		if (timeNow > hourOne){
			localCache = 0;
			//console.log('0');
		}
	});
	await ref.child("1").child("time").once('value', function (snapshot) {
		let hourTwo = new Date(snapshot.val());
		if (timeNow > hourTwo){
			localCache = 1;
			//console.log('1');
		}
	});
	await ref.child("2").child("time").once('value', function (snapshot) {
		let hourThree = new Date(snapshot.val());
		if (timeNow > hourThree){
			localCache = 2;
			//console.log('3');
		}
	});
	await ref.child("3").child("time").once('value', function (snapshot) {
		let hourFour = new Date(snapshot.val());
		if (timeNow > hourFour){
			console.log("Update required");
			localCache = 3;
			//console.log('4');
		}
	});
	await console.log(`Cache Index: ${localCache}`);
};
app.get('/', async function (req, res) {
	await checkUpdate();//the first number is the hour count.
	await console.log("Checking for updates");
	 if (localCache >= 3){ //If the local cache is on 3, update cache first
		await updateCache();
		console.log("Attempting to update");
		res.redirect('back');
		 console.log("Refreshing...");
	}
	else {
		 console.log("No update needed"); //if no update is needed, display page.
		 await ref.child(localCache).once('value', function (snapshot) {
			 //console.log(localCache);
			 let surf = snapshot.val();
			 if (typeof surf.windDirection[0].value !== 'undefined' && surf.windDirection[0].value) {
				 x = surf.windDirection[0].value;
			 } else {
				 x = 0;
			 } //Sometimes Wind direction is not given in the api request, and therefore the
			 //application crashes because it receive an undefined value.
			 //To circumvent this, i do a check to see if wind direction exists and if it doesnt ill just assign a bogus
			 //value so the application can continue

			 //Switch case to evaluate direction of wind given degrees
			 console.log('Wind Direction is '+ x);
			 switch (true) {
				 case (x < 90): //if the direction is below 90, then it is a SW wind direction
					 direction = "SW";
					 break;
				 case (x < 180): //NW wind direction
					  direction = "NW";
					 break;
				 case (x < 270):
					 direction = "NE";
					 break;
				 case (x < 360):
					 direction = "SE";
					 break;
			 }
			 console.log(x);
			 let windSpeedkts = surf.windSpeed[1].value * 1.944; //convert the given windspeed(ms) to (kts)
			 const waveArray = [
				 {
					 swellHeight: surf.swellHeight[0].value,
					 waterTemperature: surf.waterTemperature[2].value,
					 wavePeriod: surf.wavePeriod[0].value,
					 windDirection: direction, //Taking the switch case result gives the wind direction phonetically
					 windSpeed: windSpeedkts.toFixed(1),
					 time: new Date(surf.time).toLocaleString("En-GB", {timeZone: "Pacific/Auckland"})

				 }, //Present the data back to NZDT for user

			 ];
			 console.log("Rendering Page");
			 res.render('index', {
				 waveArray: waveArray //Pass the array holding our data to the webpage
			 });
		 });
	 }
});
app.listen(port, function() { //test 2
	console.log(`Waikuku Surf Report running on port ${port}`)
});


